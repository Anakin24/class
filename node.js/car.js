module.exports = class Car {
    constructor(color , engine , gear , mark , model ) {
        this.color = color;
        this.engine = engine;
        this.gear = gear;
        this.mark = mark;
        this.model = model;
    }
    getFullCarInfo() {
        return {
            mark: this.mark,
            model: this.model,
            gear: this.gear,
            engine: this.engine,
            color: this.color
        }
    }
}