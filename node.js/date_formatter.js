exports.jora = function(time) {
    let hour = time.getHours();
    let minutes = time.getMinutes();
    return `${hour} : ${minutes}`
}


exports.dayFormatter = function(date) {
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    return `${day} / ${month} / ${year}`;
}