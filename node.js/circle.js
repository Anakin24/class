module.exports = class Circle {
    constructor(radius) {
        this.radius = radius;
    }
    getRadius() {
        return this.radius
    }
    getArea() {
        return Math.PI * Math.pow(this.radius , 2);
    }
}